package com.riccmp.deliveryapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        butLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val intent = Intent(this@MainActivity, DetalleActivity::class.java)
        startActivity(intent)

    }
}
